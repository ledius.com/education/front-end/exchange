init:
	cp .env.dist .env

deploy:
	ssh -o StrictHostKeyChecking=no ${SSH_USER}@${SSH_HOST} "docker login $(value DOCKER_REGISTRY) -u $(value DOCKER_USER) -p $(value DOCKER_PASS)"
	ssh -o StrictHostKeyChecking=no ${SSH_USER}@${SSH_HOST} "[[ -d web ]] || git clone https://$(value GIT_USER):$(value GIT_PASS)@gitlab.com/hodl2/invest/front-end/web.git web"
	ssh -o StrictHostKeyChecking=no ${SSH_USER}@${SSH_HOST} "cd web && (git pull || true) && git checkout $(value APP_VERSION)"
	ssh -o StrictHostKeyChecking=no ${SSH_USER}@${SSH_HOST} "\
		export VERSION=$(value APP_VERSION) && \
		cd web && \
		docker-compose -f docker-compose.production.yml pull ledius-platform && \
		docker-compose -f docker-compose.production.yml up -d \
	"
