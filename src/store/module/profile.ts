import { Profile } from "@/domain/entity/profile/Profile";
import { Module } from "vuex";
import axios from "axios";
import { ProfileResponse } from "@/domain/response/profile/ProfileResponse";
import { ProfileCreateRequest } from "@/domain/request/profile/ProfileCreateRequest";
import { ProfileEditRequest } from "@/domain/request/profile/ProfileEditRequest";

type State = {
  profile: Profile|null;
};

export default {
  namespaced: true,
  state: {
    profile: null
  },
  getters: {
    exists: (state): boolean => state.profile !== null,
    get: (state): Profile|null => state.profile
  },
  mutations: {
    set: (state, profile: Profile): void => {
      state.profile = profile;
    }
  },
  actions: {
    create: async ({ commit }, request: ProfileCreateRequest): Promise<void> => {
      const { data } = await axios.post<ProfileResponse>('/api/v1/profiles', request);
      const profile = Profile.from(data);
      commit('set', profile);
    },
    edit: async ({ commit }, request: ProfileEditRequest): Promise<void> => {
      const { data } = await axios.put<ProfileResponse>('/api/v1/profiles', request);
      const profile = Profile.from(data);
      commit('set', profile);
    },
    fetch: async ({ commit }, userId: string): Promise<void> => {
      const { data } = await axios.get<ProfileResponse>(`/api/v1/profiles/user/${userId}`);
      const profile = Profile.from(data);
      commit('set', profile);
    }
  }
} as Module<State, unknown>;
