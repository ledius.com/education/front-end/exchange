import { Bid } from "@/domain/entity/bid/Bid";
import { Module } from "vuex";
import { BidCreateRequest } from "@/domain/request/bid/BidCreateRequest";
import axios from "axios";
import { BidResponse } from "@/domain/response/bid/BidResponse";
import { BidListRequest } from "@/domain/request/bid/BidListRequest";
import { PaymentResponse } from "@/domain/response/payment/PaymentResponse";

type State = {
  bids: Bid[];
};

export default {
  namespaced: true,
  state: {
    bids: []
  },
  getters: {
    items: (state) => state.bids
  },
  mutations: {
    add: (state, payload: Bid): void => {
      state.bids = [...state.bids, payload];
    },
    set: (state, payload: Bid[]): void => {
      state.bids = payload;
    }
  },
  actions: {
    create: async ({ commit }, request: BidCreateRequest): Promise<Bid> => {
      const { data } = await axios.post<BidResponse>('/api/v1/bid', request);
      const bid = Bid.from(data);
      commit('add', bid);
      return bid;
    },
    fetch: async ({ commit }, request: BidListRequest): Promise<void> => {
      const params = {} as Record<string, string>;
      if (request.userId) {
        params.userId = request.userId;
      }
      const { data } = await axios.get<BidResponse[]>('/api/v1/bids', { params });
      const bids = data.map(Bid.from);
      commit('set', bids);
    },
    payment: async (_, bidId: string): Promise<void> => {
      const { data } = await axios.post<PaymentResponse>(`/api/v1/bid/${bidId}/payment`);
      window.open(data.paymentUrl, "_blank");
    }
  }
} as Module<State, unknown>;
