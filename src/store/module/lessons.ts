import { Lesson } from "@/domain/entity/lesson/Lesson";
import { Module } from "vuex";
import { RootState } from "@/store";
import axios from "axios";
import { LessonListResponse } from "@/domain/response/lessons/LessonListResponse";
import { LessonsListRequest } from "@/domain/request/lessons/LessonsListRequest";

type State = {
  lessons: Lesson[];
};

export default {
  namespaced: true,
  state: {
    lessons: []
  },
  getters: {
    items: (state: State) => state.lessons
  },
  mutations: {
    set: (state: State, lessons: Lesson[]): void => {
      state.lessons = lessons;
    }
  },
  actions: {
    fetch: async ({ commit }, request: LessonsListRequest): Promise<void> => {
      const response = await axios.get<LessonListResponse>("/api/v1/lessons", {
        params: request
      });
      const lessons = response.data.items.map(item => {
        return new Lesson(item.id, item.content.name, item.content.icon, item.courseId);
      });
      commit('set', lessons);
    }
  }
} as Module<State, RootState>;
