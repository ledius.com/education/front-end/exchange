import { Module } from 'vuex';
import axios from 'axios';
import { CourseResponse, CoursesApi } from '@/api/courses';
import { configuration } from '@/api/configuration';

type State = {
  courses: CourseResponse[];
  course: CourseResponse | null;
};

const coursesApi = new CoursesApi(configuration());

export default {
  namespaced: true,
  state: {
    courses: [],
    course: null
  },
  getters: {
    items: state => state.courses,
    course: state => state.course
  },
  mutations: {
    set: (state: State, items: CourseResponse[]): void => {
      state.courses = items;
    },
    setCourse: (state: State, course: CourseResponse): void => {
      state.course = course;
    }
  },
  actions: {
    fetch: async ({ commit }): Promise<void> => {
      const courseListResponse = await coursesApi
        .list()
        .then(({ data }) => data);
      commit('set', courseListResponse.items);
    },
    fetchCourse: async ({ commit }, id: string): Promise<void> => {
      const course = await axios
        .get<CourseResponse>(`/api/v1/course/${id}`)
        .then(({ data }) => data);
      commit('setCourse', course);
    }
  }
} as Module<State, unknown>;
