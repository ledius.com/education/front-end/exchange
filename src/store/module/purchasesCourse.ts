import { PurchaseCourse } from '@/domain/entity/purchase/course/PurchaseCourse';
import { Module } from 'vuex';
import { RootState } from '@/store';
import axios from 'axios';
import { PurchasesCourseListResponse } from '@/domain/response/purchase/course/PurchasesCourseListResponse';
import { PurchasePayForm } from '@/domain/form/purchase/PurchasePayForm';
import { CourseBuyLdsCommand, CoursePurchasesApi } from '@/api/courses';
import { configuration } from '@/api/configuration';

type State = {
  purchases: PurchaseCourse[];
};

const coursePurchasesApi = new CoursePurchasesApi(configuration());

export default {
  namespaced: true,
  state: {
    purchases: []
  },
  getters: {
    items: state => state.purchases
  },
  mutations: {
    set: (state: State, payload: PurchaseCourse[]) => {
      state.purchases = payload;
    },
    push: (state: State, payload: PurchaseCourse) => {
      state.purchases = [...state.purchases, payload];
    }
  },
  actions: {
    fetch: async ({ commit }, userId: string): Promise<void> => {
      const { data } = await axios.get<PurchasesCourseListResponse>(
        `/api/v1/purchases/course?userId=${userId}`
      );
      const purchases = data.items.map(PurchaseCourse.of);
      commit('set', purchases);
    },
    pay: async ({ commit }, form: PurchasePayForm): Promise<void> => {
      const { data } = await axios.post('/api/v1/purchase/course/pay', form);
      window.open(data.paymentUrl, '_blank');
    },

    buy: async (
      { commit },
      courseBuyLdsCommand: CourseBuyLdsCommand
    ): Promise<void> => {
      const purchase = await coursePurchasesApi
        .buyLds({ courseBuyLdsCommand })
        .then(({ data }) => data);
      commit('push', purchase);
    }
  }
} as Module<State, RootState>;
