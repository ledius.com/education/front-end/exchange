import { Module } from 'vuex';
import {
  AccountResponse,
  BalanceApi,
  TransferApi,
  TransferCommand
} from '@/api/ledius-token';
import { configuration } from '@/api/configuration';
import { formatBalance } from '@/utils';

export type Balance = {
  balance: string;
  symbol: string;
};

export interface FormattedBalance extends Balance {
  formattedBalance: string;
}

export function sortLDSFirst(a: Balance): number {
  return a.symbol === 'LDS' ? -1 : 1;
}

type State = {
  balances: Record<string, FormattedBalance[]>;
};

const balanceApi = new BalanceApi(configuration());
const transferApi = new TransferApi(configuration());

export default {
  namespaced: true,
  state: {
    balances: {}
  },
  getters: {
    getBalanceForAccount: state => (account: AccountResponse) =>
      state.balances[account.address]?.sort(sortLDSFirst) || [],
    getBalanceBySymbol: state => (account: AccountResponse, symbol: string) =>
      state.balances[account.address]?.find(
        balance => balance.symbol === symbol
      )
  },
  mutations: {
    setBalance: (
      state,
      [account, balances]: [AccountResponse, FormattedBalance[]]
    ) => {
      state.balances = {
        ...state.balances,
        [account.address]: balances
      };
    }
  },
  actions: {
    fetchBalance: async (
      { commit },
      payload: AccountResponse
    ): Promise<Balance[]> => {
      const balances: Balance[] = ((await balanceApi
        .accountBalance(payload)
        .then(response => response.data)) as unknown) as Balance[];
      commit('setBalance', [
        payload,
        balances.map(
          balance =>
            ({
              ...balance,
              formattedBalance: formatBalance(balance.balance)
            } as FormattedBalance)
        )
      ]);
      return balances;
    },
    withdraw: async (_, transferCommand: TransferCommand): Promise<void> => {
      await transferApi.transfer({
        transferCommand
      });
    }
  }
} as Module<State, unknown>;
