import store from "@/store";
import { NavigationGuardNext, Route } from "vue-router";
import Vue from "vue";

export const routeAuthGuard = (to: Route, from: Route, next: NavigationGuardNext<Vue>): void => {
  if (to.matched.some(route => route.meta.requiredAuth)) {
    if (store.getters['auth/isAuth']) {
      next();
    } else {
      next({
        name: "Login"
      });
    }
  } else {
    next();
  }
};
