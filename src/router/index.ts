import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import { routeAuthGuard } from '@/router/hook/routeAuthGuard';
import { routeGuestGuard } from '@/router/hook/routeGuestGuard';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/exchange/ExchangeView.vue'),
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/signup',
    name: 'Signup',
    component: () =>
      import(/* webpackChinkName: "signup" */ '../views/signup/SignupView.vue'),
    meta: {
      requiredGuest: true
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/login/LoginView.vue'),
    meta: {
      requiredGuest: true
    }
  },
  {
    path: '/profile',
    name: 'Profile',
    component: () => import('../views/profile/ProfileView.vue'),
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/auth/:refreshToken',
    name: 'Auth',
    component: () => import('../views/auth/AuthRefreshView.vue'),
    meta: {
      requiredAuth: false
    }
  },
  {
    path: '/exchange',
    name: 'Exchange',
    component: () => import('../views/exchange/ExchangeView.vue'),
    meta: {
      requiredAuth: true
    }
  },
  {
    path: '/bank-details',
    name: 'BankDetails',
    component: () => import('../views/exchange/BankDetailsView.vue'),
    meta: {
      requiredAuth: true
    }
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach(routeAuthGuard);
router.beforeEach(routeGuestGuard);

export default router;
