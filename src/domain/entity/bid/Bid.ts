import { Consultation } from "@/domain/entity/consultation/Consultation";
import { Status } from "@/domain/entity/bid/Status";
import { BidResponse } from "@/domain/response/bid/BidResponse";

export class Bid {
  public readonly id: string;
  public readonly userId: string;
  public readonly consultation: Consultation;
  public readonly description: string;
  public readonly price: number;
  public readonly status: Status;

  constructor (id: string, userId: string, consultation: Consultation, description: string, price: number, status: Status) {
    this.id = id;
    this.userId = userId;
    this.consultation = consultation;
    this.description = description;
    this.price = price;
    this.status = status;
  }

  public static from (response: BidResponse): Bid {
    return new Bid(
      response.id,
      response.userId,
      Consultation.from(response.consultation),
      response.description,
      response.price,
      response.status
    );
  }

  public get isPending (): boolean {
    return this.status === Status.Pending;
  }

  public get isCompleted (): boolean {
    return this.status === Status.Completed;
  }

  public get isPaid (): boolean {
    return this.status === Status.Paid;
  }
}
