import { LessonResponse } from "@/domain/response/lessons/LessonResponse";

export class LessonContent {
  public readonly id: string;
  public readonly name: string;
  public readonly icon: string;
  public readonly text: string;
  public readonly courseId: string;

  constructor (id: string, name: string, text: string, icon: string, courseId: string) {
    this.id = id;
    this.name = name;
    this.text = text;
    this.icon = icon;
    this.courseId = courseId;
  }

  public static ofLessonResponse (lesson: LessonResponse): LessonContent {
    return new LessonContent(
      lesson.id,
      lesson.content.name,
      lesson.content.text,
      lesson.content.icon,
      lesson.courseId
    );
  }
}
