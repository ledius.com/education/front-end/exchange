import { WalletResponse } from '@/domain/response/wallet/WalletResponse';

export class Wallet {
  public readonly id: string;
  public readonly ownerId: string;
  public readonly amount: number;

  constructor(id: string, ownerId: string, amount: number) {
    this.id = id;
    this.ownerId = ownerId;
    this.amount = amount;
  }

  public static from(response: WalletResponse): Wallet | null {
    if (!response.wallet) {
      return null;
    }
    return new Wallet(
      response.wallet.id,
      response.wallet.ownerId,
      response.wallet.amount
    );
  }

  public getAmountInRub(): number {
    return this.amount / 100;
  }
}
