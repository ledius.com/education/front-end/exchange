export interface BidCreateRequest {
  userId: string;
  consultationId: string;
  price: number;
  description: string;
}
