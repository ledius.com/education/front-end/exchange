import { PurchaseCourseItem } from "@/domain/response/purchase/course/PurchaseCourseItem";

export interface PurchasesCourseListResponse {
  items: PurchaseCourseItem[];
}
