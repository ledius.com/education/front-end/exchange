import { WalletItem } from '@/domain/response/wallet/WalletItem';

export interface WalletResponse {
  wallet: WalletItem | null;
}
