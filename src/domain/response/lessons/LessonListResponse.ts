import { LessonItem } from "@/domain/response/lessons/LessonItem";

export interface LessonListResponse {
  items: LessonItem[];
}
