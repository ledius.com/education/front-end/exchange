import { Status } from "@/domain/entity/bid/Status";
import { ConsultationResponse } from "@/domain/response/consultations/ConsultationResponse";

export interface BidResponse {
  readonly id: string;
  readonly userId: string;
  readonly consultation: ConsultationResponse;
  readonly description: string;
  readonly price: number;
  readonly status: Status;
}
