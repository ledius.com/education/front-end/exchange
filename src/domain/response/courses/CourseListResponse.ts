import { CourseItem } from "@/domain/response/courses/CourseItem";

export interface CourseListResponse {
  items: CourseItem[];
}
