export class PurchasePayForm {
  public readonly courseId: string;
  public readonly userId: string;

  constructor (courseId: string, userId: string) {
    this.courseId = courseId;
    this.userId = userId;
  }
}
